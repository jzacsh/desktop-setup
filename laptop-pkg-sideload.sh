#!/usr/bin/env bash
set -euo pipefail
declare -r dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
source "$dir"/distro_check.sh

[[ -e ~/.tmux/plugins/tpm ]] || {
  echo "getting tmux plugins setup..."
  mkdir -vp ~/.tmux/plugins/
  git clone --quiet https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  echo "NOTE: must manually remember to C-I (for 'install') on first tmux use!" >&2
}

printf 'running distro (%s) specific logic...\n' "$distro"
case "$distro" in
  arch)
    aur_pkgs=(
      google-chrome
      drawio
      pidcat-git
      vcprompt
      neovim-symlinks
      ruby-neovim
      joplin
      joplin-desktop
      tutanota-desktop
      protonmail-bridge
      # nice tip per https://wiki.archlinux.org/title/System_maintenance#Backup
      informant

      # my keyboard
      uhk-agent-appimage

      # alternative to gnome dictionary powered by wiktionary
      quick-lookup

      # note-taking app
      logseq-desktop
    )
    # TODO remove this loop and pass it all directly to yay when there's no
    # failing packages in this list (eg pidcat-git aur package is stale in its
    # hand-editing for python shebang).
    printf 'installing %s AUR packages...\n' "${#aur_pkgs[@]}"
    i=0
    for pkg in "${aur_pkgs[@]}";do
      i=$(( i + 1 ))
      printf 'installing pkg %s of %s, package "%s"...\n' \
        "$i" "${#aur_pkgs[@]}" "$pkg"
      yay -S "$pkg" || printf \
        'FAILURE on package "%s"; carrying on to other packages\n' \
        "$pkg"
    done
    # for the informant package above
    sudo usermod -a -G informant "$USER"

    echo "TODO(install) arch world: fonts (eg golang's "Go Mono")"

    ;;

  debian)
    echo "getting vcprompt"
    curl -O http://hg.gerg.ca/vcprompt/archive/tip.tar.gz
    tar -xvf tip.tar.gz
    pushd vcprompt*
    autoconf
    ./configure
    make
    mv -v vcprompt ~/bin/local/
    popd # vcrompt-*
    rm ./vcprompt* tip.tar.gz -rf

    echo 'installing golang fonts, eg: "Go Mono"'
    git clone --quiet https://go.googlesource.com/image golang-image
    mkdir -vp ~/.local/share/fonts/
    find golang-image/font/gofont/ttfs/ \
      -mindepth 1 -maxdepth 1 -type f -name '*.ttf' \
      -exec cp -vp {} ~/.local/share/fonts/ \;
    rm -rf golang-image

    echo 'installing user borgmatic bin for backups'
    pip3 install --user --upgrade borgmatic
    # TODO this^ line (or laptop-sys-*.sh's version of this line) should be removed


    # NOTE: keep in sync with share/zacsh_exports of gitlab.com/jzacsh/bin
    export GOPATH="$HOME"/usr/local/go
    export PATH="$GOPATH"/bin:"$PATH"

    echo 'TODO(install) side-load script for `drawio` package on debian not yet written' >&2

    ################################################
    # all elevated privelege actions below this line
    ################################################

    # as of 2021-04-09, this is the LTS release
    #  Taken from
    #    https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
    #  According to
    #    https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
    printf 'preparing apt repos so nodejs is available...\n'
    sudo apt install curl
    sudo bash -c 'curl -fsSL https://deb.nodesource.com/setup_14.x | bash -' || die

    echo "refreshing font caches"
    sudo fc-cache -fv

    echo "getting typescript"
    sudo npm install -g typescript

    printf \
      'installing rclone afresh per\n\t%s\n' \
      'https://rclone.org/downloads' >&2
    ( set -x; curl https://rclone.org/install.sh | sudo bash; )

    # https://keybase.io/docs/the_app/install_linux
    echo 'installing keybase'
    (
      deb="$(mktemp --tmpdir=. 'keybase-deb_XXXXX.deb')"
      curl -s https://prerelease.keybase.io/keybase_amd64.deb > "$deb"

      set -x
      sudo apt-get install "$deb"
    )

    # https://element.io/get-started
    echo 'installing element (matrix.org) desktop client'
    sudo apt-get install -y apt-transport-https
    curl -sL https://packages.riot.im/debian/riot-im-archive-keyring.gpg |
        sudo tee /usr/share/keyrings/riot-im-archive-keyring.gpg
    echo 'deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main' |
      sudo tee /etc/apt/sources.list.d/riot-im.list
    sudo apt-get update
    sudo apt-get install -y element-desktop

    # printf 'TODO(install): run `opam init` once!' >&2
    # printf 'TODO(install): run `opam install ocp-indent` once!' >&2

    echo 'TODO(install): go install github.com/jurplel/qView via AppImage'

    echo "TODO(install) go run rbenv doctor and see what's left: github.com/rbenv/rbenv#using-package-managers"

    echo "TODO(install) once rbenv doctor passes, go install software via gem packages: laptop-post-rbenv.sh"

    echo 'TODO(install) go setup signal.org chat per signal.org/en/download'
    # (
    #
    #   signalKeyring="$(mktemp --tmpdir=. 'signal-desktop-keyring_XXXXXX.gpg')"
    #   curl -s https://updates.signal.org/desktop/apt/keys.asc |
    #       gpg --dearmor >  "$signalKeyring"
    #   sudo mv -v "$signalKeyring" /usr/share/keyrings/signal-desktop-keyring.gpg
    #   echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |
    #       sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
    #   sudo apt-get update
    #   sudo apt-get install -y signal-desktop
    # )
    #
    # TODO: for some reason Pop_OS fresh install (with updates applied/reboots/etc.
    # --> ie: at the above results in "20.04 LTS") with the above command results in
    # the following on `apt-get update`:
    #     $ sudo apt-get update
    #     Hit:1 http://us.archive.ubuntu.com/ubuntu focal InRelease
    #     Get:2 http://us.archive.ubuntu.com/ubuntu focal-security InRelease [109 kB]
    #     Get:3 https://updates.signal.org/desktop/apt xenial InRelease [2,316 B]
    #     Hit:4 https://deb.nodesource.com/node_14.x focal InRelease
    #     Hit:5 http://prerelease.keybase.io/deb stable InRelease
    #     Hit:6 http://ppa.launchpad.net/system76/pop/ubuntu focal InRelease
    #     Get:7 http://us.archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]
    #     Hit:8 http://apt.pop-os.org/proprietary focal InRelease
    #     Get:9 http://us.archive.ubuntu.com/ubuntu focal-backports InRelease [101 kB]
    #     Err:3 https://updates.signal.org/desktop/apt xenial InRelease
    #      The following signatures couldn't be verified because the public key is not available: NO_PUBKEY D980A17457F6FB06
    #     Hit:10 http://ppa.launchpad.net/yubico/stable/ubuntu focal InRelease
    #     Hit:11 https://packages.riot.im/debian default InRelease
    #     Hit:12 http://dl.google.com/linux/chrome/deb stable InRelease
    #     Reading package lists... Done
    #     W: GPG error: https://updates.signal.org/desktop/apt xenial InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY D980A17457F6FB06
    #     E: The repository 'https://updates.signal.org/desktop/apt xenial InRelease' is not signed.
    #     N: Updating from such a repository can't be done securely, and is therefore disabled by default.
    #     N: See apt-secure(8) manpage for repository creation and user configuration details.
    #
    #jzacsh@ rm'd as no longer using snap excessively
    #sudo snap install signal-desktop

    # Install Syncthing
    #
    # upstream info has to be followed for debian/ubuntu:
    #   https://docs.syncthing.net/users/contrib.html#packages-and-bundlings
    #
    # NOTE: this isn't needed in the archlinux branch of code above, cuz we can
    # rely on good packages out of the box
    sudo curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
    echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
    sudo apt update
    sudo apt install syncthing

    ;;
esac

printf 'TODO(install): golang not installed; run then:\n'
printf '\tmanually run ./laptop-post-golang.sh\n'
printf '\tmanually run `:GoInstallBinaries` in vim\n'

printf 'TODO(install) in a fresh shell (ie your real environment): https://rustup.rs/\n'



# setup syncthing now that it's installed (possible officially, possible via
# side-loading, depending on the OS).
systemctl --user stop syncthing.service
# Before:
#    <folder id="default" label="Default" path="~/Sync" type="s...
# After:
#   <folder id="default" label="Default" path="~/sync/dweb/syncthing/content/default" type="s...
sed -i 's,\(\s*<folder.*id="default".*path="\)~/Sync",\1~/sync/dweb/syncthing/content/",g' ~/.config/syncthing/config.xml
sed -i 's,\(\s*<folder.*id="default".*path="\)'"$HOME"'/Sync",\1~/sync/dweb/syncthing/content/",g' ~/.config/syncthing/config.xml
systemctl --user start syncthing.service

printf 'TODO(install) fix <defaults> of ~/.config/syncthing/config.xml to point to ~/sync/dweb/syncthing/content/ (after stopping the service first!)\n'
