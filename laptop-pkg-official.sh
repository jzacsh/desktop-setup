#!/bin/bash
declare -r dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
source "$dir"/distro_check.sh

pkgList() (
  grep --invert-match --extended-regexp '^#' "$packageList" |
    sed -e 's|\s*#.*$||g' |
    sed '/^\s*$/'d |
    sort |
    uniq
)

pkg_mgr_update() {
  # see https://wiki.archlinux.org/title/Pacman/Rosetta for a nice mapping
  case "$distro" in
    arch)
      sudo pacman -Syu
      ;;
    debian)
      sudo apt update
      ;;
  esac
}

pkg_mgr_install() {
  set -x
  # see https://wiki.archlinux.org/title/Pacman/Rosetta for a nice mapping
  case "$distro" in
    arch)
      sudo pacman -S --needed $@ # no quotes; multi-arg passing to apt-get
      ;;
    debian)
      sudo apt install $@ # no quotes; multi-arg passing to apt-get
      ;;
  esac
  set +x
}
packageList="$(
  readlink -f "$dir"/"$distro".list || {
    printf 'ERROR: failed to find package list for distro "%s"\n' "$distro" >&2
    exit 1
  }
)"; declare -r packageList; [[ -r "$packageList" ]]

finalList="$(readlink -f "$(mktemp --tmpdir=. 'dpkg_list.XXXXXX.txt')")"
printf 'writing install list to\n\t%s\n' "$finalList"
pkgList > "$finalList"
pkg_mgr_update
pkg_mgr_install $(printf ' %s ' "$(pkgList)") # no quotes; multi-arg passing to apt-get
pkg_mgr_update
