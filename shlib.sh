#!/usr/bin/env bash

function dief() {
  local fmt='FATAL: '
  if [[ "$#" -eq 0 ]]; then
    fmt="${fmt}error, see above.\n"
  else
    fmt="${fmt}${1}"
    shift
  fi
  printf -- "$fmt" "$@" >&2
  exit 1
}

# adapted from https://gitlab.com/jzacsh/yabashlib/-/blob/c8f84ad2efdcf/src/unixy.sh
function __getLsbReleaseVal() {
	local key="$1"

	local awkScript; printf -v awkScript -- \
		'$1 == "%s" {print tolower($2)}' \
  	"$key"

  awk -F'=' \
		"$awkScript" \
		/etc/lsb-release
}

# adapted from https://gitlab.com/jzacsh/yabashlib/-/blob/c8f84ad2efdcf/src/unixy.sh
function is_distro_nixos() {
	local distro
	distro="$(__getLsbReleaseVal DISTRIB_ID)" && [[ "$distro" = nixos ]]
}
