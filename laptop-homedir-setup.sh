#!/usr/bin/env bash

selfd="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" || {
  printf 'somehow missing BASH_SOURCE\n' >&2
  exit 1
}; declare -r selfd
source "$selfd"/shlib.sh ||
  dief 'cannot source %s\n' "$selfd"/shlib.sh
source "$selfd"/distro_check.sh || 
  dief 'cannot source %s\n' "$selfd"/distro_check.sh

declare -r opt_useJJ=1

echo 'cloning personal dot, bin repos and other stuff...'
# $1=gitlab "username/repo" string to clone
# $2=directory to clone into
function cloneOwnedRepo() (
  local readonlyRepoUrl="https://gitlab.com/$1"
  local mutableRepoUrl=git@gitlab.com:"$1"
  local dest="$2"
  if (( opt_useJJ )); then
    # TODO: update when/if jj gets the ability to have multiple distinct URLs
    # for a single remote.
    jj git clone "$readonlyRepoUrl" "$dest" &&
      jj git remote set-url origin "$mutableRepoUrl"
    return $?
  else
    git clone --quiet "$readonlyRepoUrl" "$dest" &&
      cd "$dest" &&
      git remote set-url origin --push "$mutableRepoUrl"
    return $?
  fi
)
cd # home
declare -ar privateDots=(
  .gnupg/*
  .ssh/config
  .aws/config
)
# TODO: consider moving from ~/usr/ to already widely used ~/.local/; eg:
# ~/usr/lib -> ~/.local/lib
# ~/usr/local/source -> ~/.local/source
# ~/usr/bin -> ~/.local/bin
# ~/usr/share/appimage -> ~/.local/share/$USER-appimage (or betteR: ~/.local/bin)
declare -ra nonXdgDirs=(
  back/
  .gnupg/
  tmp/{build,src}
  usr/{local/source,log/arch,share{,/appimage},lib/{,go,sh,deno,android/adk,rust/{cargo,rustup}}}
  .config/
)
mkdir -p ${nonXdgDirs[@]} # Unpack my bag o scripts...
chmod 7777 tmp/
mkdir -p bin/local
cloneOwnedRepo jzacsh/dotfiles back/dots

function mvIfPwdOrig() (
  local orig; orig="$(basename "$1")"
  [[ -e "$orig" ]] || return 0

  printf '\tWARNING: moving existing file, "%s"\n' "${orig}" >&2
  mv --verbose "$orig"{,.orig}
)

echo 'organizing furniture: removing default dirs in favor of $XDG dirs...'
rmdir ~/{Desktop,Pictures,Downloads,Music,Documents,Videos,Templates,Public}/ ||
  echo 'WARNING: some default dirs might have not been empty; carrying on...' >&2

# TODO: pull this out to a re-callable, idempotent script inside of
# .host/common/ of gitlab.com/jzacsh/dotfiles
while read -r line; do
  [[ "$line" =~ ^XDG ]] || continue  # skip; not a DIR configuration

  tmp="${line/XDG*=/}"  # get the config value

  # strip surrounding quotes
  tmp="${tmp%\"}"
  tmp="${tmp#\"}"

  # eval vars. eg: tmp='$HOME/foo' -> '/home/jzacsh/foo'
  dir="$(eval echo -n "$tmp")" ||
    dief 'failed eval of xdg-dirs line "%s"\n' "$tmp"

  # TODO: symlink to $XDG_DOCUMENTS_DIR
  #   cd $XDG_DOCUMENTS_DIR
  #   ln -sv /usr/share/doc system

  mkdir -p "$dir"
done < ~/back/dots/.config/user-dirs.dirs

# get my own source directory started
mkdir -p ~/media/src/

[[ -e ~/usr/local/source/README.md ]] || cat > ~/usr/local/source/README.md <<EOF
# jzacsh@'s build & install dir

To ensure install prefixes don't polute each other this directory is intended
for each project to have its own dir with an 'install' and 'src' subdirectory.
Other dotfiles will detect said subdir of 'install' and then add those to
\$PATH when available.

Example subdirs might look like:
  nvim/
  nvim/helper-script.sh # hypothetical 1-2 line helpers to make udpates easier
  nvim/src/             # repo hand-downloaded by you, so you can build to:
  nvim/install/...      # populated by `make` and other build tools
  nvim/install/bin/...  # populated by `make` and other build tools
EOF

mkdir -p ~/sync/{dweb{,/syncthing/{,config,content}},pesos/{,config,content,datadumps}}
pushd ~/.config; ln -sv ~/sync/dweb/syncthing/config syncthing; popd
[[ -e ~/sync/README.md ]] ||
  echo 'This directory contains files that help me organize content I own in some
respect. The categories are:
- [`PESOS`: Publish Elsewhere, Syndicate (to your) Own Site][PESOS]
- [`POSSE`: Publish (on your) Own Site, Syndicate Elsewhere,][POSSE]
- `dweb`: distributed web where this machine is _one_ participant in a distributed
    network of machines.

[POSSE]: https://indieweb.org/POSSE
[PESOS]: https://indieweb.org/PESOS
' > ~/sync/README.md

while read dot; do
  f="$(basename "$dot")"

  [[ "$f" = .config ]] && continue # skip; see next section

  mvIfPwdOrig "$dot"

  ln --symbolic --verbose "$dot"
done < <(find ~/back/dots/ -maxdepth 1 -mindepth 1 -name '\.*' -print)
# deal w/manual imperfections of a simple dots directory....
rm .git  # symlink to .git/ repo directory of ~/back/dots/
rm .gitignore # this is .gitignore for ~/back/dot/ *repo*
mv .gitignore{_global,}  # *this* is the gitignore for $HOME

pushd ~/.config/ # map tracked ~/.config/ content to local .config dir on disk
while read configFile; do
  mvIfPwdOrig "$configFile"
  ln --symbolic --verbose "$configFile"
done < <(find ~/back/dots/.config/ -mindepth 1 -maxdepth 1 -print)
popd # from ~/.config/

chmod 700 .gnupg/ # should be rw (and executable) only to you
pushd ~/.gnupg/
while read gpgconf; do
  mvIfPwdOrig "$gpgconf"
  ln --symbolic --verbose "$gpgconf"
done < <(find ~/.config/ -mindepth 1 -maxdepth 1 -name 'gpg*.conf' -print)
popd # from ~/.gnupg/

is_distro_nixos || {
  function current_default_shell() { getent passwd "$USER" | awk -F: "{print $7}"; }
  [[ "$(basename "$(current_default_shell)")" = bash ]] || (
    bash_exec="$(type -p bash)"
    printf 'ensuring default shell is bash (at %s) for this user...\n' "$bash_exec"
    set -x
    chsh -s "$bash_exec"
  )
}

echo "Finished; Now logging stuff that can't be public or automated..."
printf 'TODO(install): go install:\n\t%s\n' ${privateDots[@]}
