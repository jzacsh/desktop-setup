#!/usr/bin/env bash
set -euo pipefail
declare -r dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
source "$dir"/distro_check.sh

adminUsr="$1"; declare -r adminUsr
# sanity checks
if [[ -z "${adminUsr/ */}" || "$adminUsr" = root ]];then
  printf "Error: must provide USER arg (machine's effective owner)!\n" >&2
  exit 1
fi
[[ "$EUID" -eq 0 ]] || {
  printf 'Error: called incorrectly; must be run as root!\n' >&2
  exit 1
}

echo 'systemd: Being primary user, we likely have long running jobs, multiplexers open, etc...'
loginctl enable-linger "$adminUsr"

echo 'systemd: In a similar vein: do not kill when we have logouts...'
cp -v /etc/systemd/logind.conf{,_"$(date --iso-8601=d)".orig}
sed -i 's/^[[:space:]]*#\(KillUserProcesses\)=.*$/\1=no/' /etc/systemd/logind.conf

# per: https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs
echo 'systemd: enabling persistent cross-boot journalctl logs...'
[[ -d /var/log/journal ]] || mkdir /var/log/journal
if ! grep -E '^Storage=persistent$' /etc/systemd/journald.conf >/dev/null 2>&1;then
  cp -v /etc/systemd/journald.conf{,.orig-"$(date --iso-8601)"}
  sed --in-place \
    's/^#\?\(Storage\)=.*$/\1=persistent/g' \
    /etc/systemd/journald.conf
fi

#TODO consider deleting this block - haven't had this problem in quite a while.
#
## ensure both aspects of plug/unplug of a headphone/mic set behave as expected
##
## https://askubuntu.com/a/158250
## https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-switch-on-connect
#declare -r audioConf=/etc/pulse/default.pa
#if [[ -s "$audioConf" ]];then
#  printf 'TODO(install) fix headphone plug/unplug by doing:
#    1) open %s file
#    2) follow instructions in comments (e.g. importance of order of modules being loaded)
#    3) ensure you have _both_ these `load-module` lines:
#      %s
#      %s
#    More at https://askubuntu.com/a/158250\n' \
#    "$audioConf" \
#    'load-module module-switch-on-connect' \
#    'load-module module-switch-on-port-available'
#else
#  echo 'TODO(install) ensure headphone plug/unplug works; apparently not using pulseaudio right now'
#fi

#2021-05-29 disabled as this consistently breaks thinkpad trackpoint system (and
#doesn't work universally anyway - e.g., in XFCE this has no impact)
#
# naturalScrollingConf=/usr/share/X11/xorg.conf.d/11-natural-scrolling.conf
# declare -r naturalScrollingConf
# if [[ -e "$naturalScrollingConf" ]];then
#   printf \
#     'WARNING: moving pre-existing conf out of way! Double check its contents:\n\t%s\n' \
#     "$naturalScrollingConf" >&2
#   mv -v "$naturalScrollingConf"{,.orig}
# fi
# cat > "$naturalScrollingConf" <<END_OF_NATURAL_SCROLLING_CONF
# # jzacsh@ manual injected this file (via
# #  gist.github.com/jzacsh/21ff8a17e041f8c91009e6f07c675070 script)
# # Following comment in: https://bugzilla.gnome.org/show_bug.cgi?id=682457#c35
# Section "InputClass"
#         Identifier "natural scrolling for mouse wheel"
#         MatchIsPointer "on"
#         MatchDevicePath "/dev/input/event*"
#         Driver "mouse"
#         Option "Device" "/dev/input/mice"
#         Option "Protocol" "Auto"
#         Option "ZAxisMapping" "5 4"
# EndSection
# END_OF_NATURAL_SCROLLING_CONF

#actually this is a beautiful thing; only wanna kill this when i'm in a special
#system and i'm half-ass using gnome, but not _really_ using gnome
# gnomeSshKeyring=/etc/xdg/autostart/gnome-keyring-ssh.desktop
# declare -r gnomeSshKeyring
# if [[ -e "$gnomeSshKeyring" ]];then
#   mv -v "$gnomeSshKeyring"{,_disabled-"$(date --iso-8601)"}
# fi

# remove deja-dup software; wouldn't have even noticed the software installed if
# i didn't find it eating my CPU...
#apt-get remove deja-dup
echo 'TODO(install): consider if deja-dup is installed and auto-running!'

# disable "tracker" software eating my CPU/mem, and unwanted anyway
#    per https://askubuntu.com/a/348692/426803
#
# NOTE: "hidden" means deleted; see https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#key-hidden
echo -e "\nHidden=true\n" | sudo tee --append \
  /etc/xdg/autostart/tracker-{extract,store,miner-{apps,fs,user-guides}}.desktop > /dev/null
gsettings set org.freedesktop.Tracker.Miner.Files crawling-interval -2
gsettings set org.freedesktop.Tracker.Miner.Files enable-monitors false
tracker reset --hard

printf \
  'TODO(install): GO INSTALL tomb command from tar release:\n\t%s\n' \
  'github.com/dyne/Tomb/blob/master/INSTALL.md#install-tomb' >&2

printf 'WARNING: IF librem13, UNCOMMENT last lines your touchpad!\n' >&2
#printf '%s\n' \
#  '#!/bin/bash' \
#  '# jzacsh 20170921, taken from: https://forums.puri.sm/t/byd-touchpad-multi-touch-support/915/4' \
#  'echo -n byd > /sys/bus/serio/devices/serio1/protocol' | tee /etc/rc.local
#chmod 744 /etc/rc.local

if [[ "$distro" = arch ]]; then
  delcare -r fav_term_emulator=/usr/bin/alacritty
else
  delcare -r fav_term_emulator=/usr/bin/urxvtc
fi

case "$distro" in
  arch)
    # initial setup for etckeeper see https://wiki.archlinux.org/title/System_maintenance#Backup
    etckeeper init
    etckeeper_init_msg="etckeep init commit by desktop-setup scripts

this comit is autogenerated _just_ after running 'etckeeper init'[1] by "$USER"
running the desktop-setup scripts at '$(git rev-parse HEAD)' from:
'''
> git remote -v
$(git remote -v)
'''

[1]: some background on motivation/etc.: https://wiki.archlinux.org/title/System_maintenance#Backup
"
    etckeeper commit -m "$etckeeper_init_msg"
    sudo systemctl enable etckeeper.timer

    echo 'TODO(install) archlinux: need adm in this world? try and see if you run into thsi'

    echo 'TODO(install) archlinux: update-alternatives version for arch?  terminal: '"$fav_term_emulator"

    echo 'TODO(install) archlinux: way to manage dns preferences nicely? just hack about files like this or maybe aur package?'

  echo 'TODO(install) archlinux: update-alternatives version for arch? gpg pinentry-curses'
    ;;
  debian)
    usermod --append --groups adm "$adminUsr"

    # Set urxvtc as my preferred terminal
    update-alternatives --install \
      /usr/bin/x-terminal-emulator x-terminal-emulator \
      /usr/bin/urxvtc 100
    update-alternatives --set x-terminal-emulator "$fav_term_emulator"

    # prefer google's public dns servers
    if [[ -d /etc/resolvconf/resolv.conf.d ]];then
      resolvConfBase=/etc/resolvconf/resolv.conf.d/base
      if [[ -r "$resolvConfBase" ]] &&
         [[ "$(stat --printf '%s\n' "$resolvConfBase")" != '0' ]];then
        cp -v "$resolvConfBase"{,_"$(date --iso-8601=d)".orig}
      fi
      printf 'nameserver 8.8.8.8\nnameserver 8.8.4.4\n' > "$resolvConfBase"
    fi

    echo 'NOTE: giving you a chance to switch pinentry to curses now:'
    update-alternatives --config pinentry
    ;;
esac

# Setup deja-dup (UI for duplicity); others to consider if needed:
# https://wiki.archlinux.org/title/Synchronization_and_backup_programs#File-based_increments
echo 'setting local, on-device backups for both system-level and user-level backups'

# Setup the /backup/... tree
sudo mkdir -p /backup/system{,/duplicity}/

# Give appropriate permissions
echo 'TODO(install) ensure you mount a non-encrypted partition for user-backups to land in /backup/user/$USER (eg for encrypted borgbackups to land)'

# Document the main /backup/ directory itself
printf '%s\n' '# local-only convenience backups for @'"$(uname -n)" '' \
    'These are local-only backups setup by jzacsh@ '"$(date --iso-8601=s)"' for different uses' \
    '' \
    'user/' \
    '    Mounted non-LUKS space for user-owned backups of personal content (eg: encrypted borgbackup of their entire $HOME, as a appropriate in sub-folders named after them, of course)' \
    'system/' \
    '    System-wide backups of non-user content (eg: of / but with /home/ excluded)' |
      sudo tee -a /backup/README.md

# Document the main /backup/system/ subdirectory
printf '%s\n' '# system-wide backups for @'"$(uname -n)" '' \
    'rsnapshot/' \
    '    these system backups were setup by jzacsh@ '"$(date --iso-8601=s)"' via rsnapshot' |
      sudo tee -a /backup/system/README.md

# TODO just write this directly to /etc/rsnapshot.conf instead of printing it
# here
echo 'TODO(install) please go setup /etc/rsnapshot.conf; here is an example of a working setup (difed against archlinux rsnapshot package)'
echo '
diff --git a/rsnapshot.conf b/rsnapshot.conf
index b70eb99..6fddae0 100644
--- a/rsnapshot.conf
+++ b/rsnapshot.conf
@@ -20,7 +20,7 @@ config_version	1.2
 
 # All snapshots will be stored under this root directory.
 #
-snapshot_root	/.snapshots/
+snapshot_root	/backup/system/rsnapshot/
 
 # If no_create_root is enabled, rsnapshot will not automatically create the
 # snapshot_root directory. This is particularly useful if you are backing
@@ -37,7 +37,7 @@ snapshot_root	/.snapshots/
 #
 # See the README file or the man page for more details.
 #
-#cmd_cp		/usr/bin/cp
+cmd_cp		/usr/bin/cp
 
 # uncomment this to use the rm program instead of the built-in perl routine.
 #
@@ -60,11 +60,11 @@ cmd_logger	/usr/bin/logger
 # If you have an older version of "du", you may also want to check the
 # "du_args" parameter below.
 #
-#cmd_du		/usr/bin/du
+cmd_du		/usr/bin/du
 
 # Uncomment this to specify the path to rsnapshot-diff.
 #
-#cmd_rsnapshot_diff	/usr/local/bin/rsnapshot-diff
+cmd_rsnapshot_diff	/usr/bin/rsnapshot-diff
 
 # Specify the path to a script (and any optional arguments) to run right
 # before rsnapshot syncs files
@@ -90,10 +90,10 @@ cmd_logger	/usr/bin/logger
 # e.g. alpha, beta, gamma, etc.         #
 #########################################
 
-retain	alpha	6
-retain	beta	7
-retain	gamma	4
-#retain	delta	3
+retain	boot	10
+retain	hourly	8
+retain	daily	8
+retain	weekly	10
 
 ############################################
 #              GLOBAL OPTIONS              #
@@ -169,12 +169,13 @@ lockfile	/var/run/rsnapshot.pid
 #
 #include_file	/path/to/include/file
 #exclude_file	/path/to/exclude/file
+exclude_file	/backup/system/rsnapshot.excludes
 
 # If your version of rsync supports --link-dest, consider enabling this.
 # This is the best way to support special files (FIFOs, etc) cross-platform.
 # The default is 0 (off).
 #
-#link_dest	0
+link_dest	1
 
 # When sync_first is enabled, it changes the default behaviour of rsnapshot.
 # Normally, when rsnapshot is called with its lowest interval
@@ -183,13 +184,13 @@ lockfile	/var/run/rsnapshot.pid
 # and all interval calls simply rotate files. See the man page for more
 # details. The default is 0 (off).
 #
-#sync_first	0
+sync_first	1
 
 # If enabled, rsnapshot will move the oldest directory for each interval
 # to [interval_name].delete, then it will remove the lockfile and delete
 # that directory just before it exits. The default is 0 (off).
 #
-#use_lazy_deletes	0
+use_lazy_deletes	1
 
 # Number of rsync re-tries. If you experience any network problems or
 # network card issues that tend to cause ssh to fail with errors like
@@ -223,9 +224,10 @@ lockfile	/var/run/rsnapshot.pid
 ###############################
 
 # LOCALHOST
-backup	/home/		localhost/
-backup	/etc/		localhost/
-backup	/usr/local/	localhost/
+backup	/		localhost/
+#backup	/home/		localhost/
+#backup	/etc/		localhost/
+#backup	/usr/local/	localhost/
 #backup	/var/log/rsnapshot		localhost/
 #backup	/etc/passwd	localhost/
 #backup	/home/foo/My Documents/		localhost/
'

# TODO move this to archlinux branch and do this work as an aur package
echo 'TODO(install) setup and enable systemd units for rsnapshot, eg:'
echo '

sudo cp -v rsnapshot*.{service,timer} /etc/systemd/system/
sudo cp -v rsnapshot.excludes /backup/system/rsnapshot.excludes
sudo systemctl enable --now rsnapshot-boot.service
sudo systemctl enable --now rsnapshot-{daily,weekly,hourly}.timer

# NOTE: sync_first usage means you need to go manually run backups first via the
# `sync` command, just this once!
sudo rsnapshot sync
'
