#!/bin/bash
if [[ "$USER" = root ]]; then
  printf 'error: this script intended for non-root user; got $USER="%s"\n' \
    "$USER" >&2
  exit 1
fi
declare -r this_dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
source "$this_dir"/distro_check.sh
[[ "$distro" = arch ]] || {
  echo 'podman setup logic only available for arch so far' >&2
  exit 1
}
_IS_ROOTLESS_CONF=0

sudo pacman -S podman{,-compose}

printf -- '%s\n' \
  '# added by hand on '"$(date --iso-8601=ns) by $USER"' per https://github.com/containers/podman/issues/9390#issuecomment-970305169' \
    'unqualified-search-registries=["docker.io"]' |
    sudo tee -a /etc/containers/registries.conf.d/docker.conf

if (( _IS_ROOTLESS_CONF ));then
  # follows https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md

  printf 'complete rootless setup instructions\n' >&2 # TODO complete this
  exit 1 # TODO complete this
fi

sudo systemctl enable podman.service
sudo systemctl start podman.service
