#!/bin/bash
if [[ "$USER" = root ]]; then
  printf 'error: this script intended for non-root user; got $USER="%s"\n' \
    "$USER" >&2
  exit 1
fi
declare -r this_dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
source "$this_dir"/distro_check.sh
[[ "$distro" = arch ]] || {
  echo 'docker setup logic only available for arch so far' >&2
  exit 1
}
_IS_ROOTLESS_CONF=0

sudo pacman -S docker{,-compose}

if (( _IS_ROOTLESS_CONF ));then
  # per https://docs.docker.com/engine/security/rootless
  sudo usermod --add-subuids 200000-201000 --add-subgids 200000-201000 "$USER"
  sudo pacman -S fuse-overlayfs
  sudo sysctl --system

  # per https://wiki.archlinux.org/title/Docker#Docker_rootless
  systemctl --user enable --now docker.socket
  yay -S docker-rootless-extras-bin
  export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/docker.sock"
  printf '%s\n' \
    '# inserted '"$(date --iso-8601=ns)"' via desktop-setup scripts by '"$USER"' per https://wiki.archlinux.org/title/Docker#Docker_rootless' \
    'export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/docker.sock"' >> ~/.host/host."$(uname -n)"

  printf 'complete rootless setup instructions\n' >&2 # TODO complete this
  exit 1 # TODO complete this
else
  sudo usermod -a -G docker "$USER"
fi

sudo systemctl enable docker.service
sudo systemctl start docker.service
