#!/usr/bin/env bash
#
# steps:
#  1. assume: $USER is primary on machine (eg: owner of a laptop)
#  2. assume: system has been set to debian sid (eg: prefer bleeding edge) or archlinux
#  3. "$1" is a script to install $HOME comforts
#  4. "$2" is a script to install distro's own official package management software targets
#  5. "$3" is a fragile script for side-installing
#     (eg: delicate from-source installations, each likely project specific)
#  6. "$4" is fragile script to custom-configure system
#     (eg: "natural scrolling" via Xorg confs, or disabling tap-to-click)
(( BASH_VERSINFO[0] < 4 )) && exit 99  # panic and tear hair out

declare -ra setupSteps=(
  'ASSUMING this is primarily your machine'
  'ASSUMING this is a debian sid machine (bleeding edge software) or archlinux'
  'use HOMEDIR_SETUP to setup $HOME dir layout, including bin/ and dotfiles'
  'use PKG_MGMT_OFFICIAL to utilize package manager for officially installing packages (from $distro.list file, one per line)'
  'use SIDE_LOAD_SH to get other binaries (eg: compile from tarballs, use pip, cpan, etc.)'
  'use SYS_CONFIG_SH to custom-configure system settings (eg: periphs, groups, xor, etc.)'
)

declare -r selfd; selfd="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" || {
  printf 'somehow missing BASH_SOURCE\n' >&2
  exit 1
}
source "$selfd"/shlib.sh ||
  dief 'cannot source %s\n' "$selfd"/shlib.sh
source "$selfd"/distro_check.sh ||
  dief 'cannot source %s\n' "$selfd"/distro_check.sh

type git >/dev/null 2>&1 ||
  dief 'missing dep: `git` must be in $PATH to start\n'

# commandline usage interaction
[[ $# -eq 4 ]] || dief \
    '`usage:  HOMEDIR_SETUP PKG_MGMT_OFFICIAL SIDE_LOAD_SH SYS_CONFIG_SH` will (in this order) do:\n%s\n\n' \
    "$(printf '\t- "%s"\n' "${setupSteps[@]}")"

homeDirSetup="$(readlink -f "$1")" ||
  dief 'missing arg HOMEDIR_SETUP\n'
declare -r homeDirSetup; [[ -x "$homeDirSetup" ]]

pkgsOfficial="$(readlink -f "$2")" ||
  dief 'missing arg PKG_MGMT_OFFICIAL\n'
declare -r pkgsOfficial; [[ -x "$pkgsOfficial" ]]

sideLoad="$(readlink -f "$3")" ||
  dief 'missing arg SIDE_LOAD_SH\n'
declare -r sideLoad; [[ -x "$sideLoad" ]]

sysConfig="$(readlink -f "$4")" ||
  dief 'missing arg SYS_CONFIG_SH\n'
declare -r sysConfig; [[ -x "$sysConfig" ]]

stepCompleted=0
function reportProgress() (
  (( stepCompleted == 6 )) && {
    printf 'SUCCESS: apparently achieved all %s steps\n' "${#setupSteps[@]}" >&2
    return 0
  }
  local step

  printf \
    'WARNING: exiting EARLY; only finished step %s of %s; summary:\n' \
    "$stepCompleted" "${#setupSteps[@]}"

  local stepStatus i=1
  for step in "${setupSteps[@]}";do
    if (( i <= stepCompleted ));then
      stepStatus='[DONE]'
    else
      stepStatus='[FAILED]'
    fi

    printf '\t%.10s  %s of %s:\t"%s"\n' \
        "$stepStatus" "$i" "${#setupSteps[@]}" \
        "${setupSteps[${i}-1]}" >&2

    i=$(( i + 1 ))
  done
)
trap reportProgress exit

function logStep() {
  set +x
  stepCompleted=$(( $1 - 1 ))
  if (( stepCompleted ));then
    printf '[FINISHED] %s of %s:\n\t%s\n\n' \
        "$stepCompleted" "${#setupSteps[@]}" \
        "${setupSteps[${stepCompleted}-1]}" >&2
    sudo --remove-timestamp # clear previously earned creds
  fi

  printf '[STARTING] %s of %s:\n\t%s\n' \
      "$1" "${#setupSteps[@]}" "${setupSteps[${1}-1]}" >&2
  set -x
}

DEBUG=0 # change this value freely!
if (( DEBUG ));then
  echo 'WARNING: this is debug mode!' >&2
  mv()       ( echo "[debug]mv"      "$@"; )
  rm()       ( echo "[debug]rm"      "$@"; )
  mkdir()    ( echo "[debug]mkdir"   "$@"; )
  ln()       ( echo "[debug]ln"      "$@"; )
  sudo()     ( echo "[debug]sudo"    "$@"; )
  crontab()  ( echo "[debug]crontab" "$@"; )
fi


logStep 1 # assume primary machine
logStep 2 # assume debian sid or archlinux


logStep 3 # layout homedir
"$homeDirSetup"

logStep 4 # install distro's software packages
"$pkgsOfficial"

logStep 5 # side load software
"$sideLoad"

logStep 6 # general system configuration prefs
sudo "$sysConfig" "$USER"

stepCompleted=0
