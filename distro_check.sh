#!/bin/bash
#
# generic distro-specific config; copy/pasted from .host/pick script of
# gitlab.com/jzacsh/dotfiles repo.
#

if [[ "${BASH_SOURCE[0]}" = "$0" ]];then
  printf 'Error: distro_check.sh intended as a source, not an invokable script!\n' >&2
  exit 1
fi

if [[ -f /etc/arch-release ]]; then
  distro=arch
elif [[ -f /etc/lsb-release ]]; then
  distro="$(awk -F'=' '/^DISTRIB_ID/{print tolower($2)}' /etc/lsb-release)"
elif [[ -f /etc/debian_version ]];then
  distro=debian
fi

case "$distro" in
  arch|debian) ;;
  nixos)
    printf 'WARNING: %s distro handling is still experimental; continuing...\n' "$distro" >&2
    ;;
  *)
    printf 'ERROR: distro "%s" not yet considered for this script\n' "$distro" >&2
    exit 1
    ;;
esac
