#!/usr/bin/env bash
set -euo pipefail

function goInstallExplained() {
  local url="$1" explainer="$2"
  printf 'trying to fetch %s...\n' "$explainer"
  go install "$url" ||
    printf \
      'fatal: build of %s (%s) failed; moving on....\n' \
      "$url" "$explainer" \
      >&2
}

goInstallExplained \
  github.com/gohugoio/hugo@latest \
  'trying to fetch hugo golang binaries...'

goInstallExplained \
  github.com/ericchiang/pup@latest \
  'trying to fetch HTML cli parser'

goInstallExplained \
  gitlab.com/jzacsh/punch/cmd/punch@latest \
  'trying to fetch punch timer tracker'

goInstallExplained \
  gitlab.com/jzacsh/runonchange@latest \
  'trying to fetch runonchange'

goInstallExplained \
  golang.org/x/tools/cmd/godoc@latest \
  'trying to fetch godoc cli'

goInstallExplained \
  github.com/loov/goda@latest \
  'trying to fetch golang dependency analysis tool'
